## Setup environment on Windows 10
1. install truffle globally `npm install -g truffle`
1. create directory
1. call `truffle init`
1. write some contract in .sol files
1. compile via `truffle compile`. Truffle has problems on Windows with the console client, but the Terminal built into VS Code seems to work fine
1. you will also need to install `web3` via `npm install web3 --save`. Then you communicate via http
1. Most likely the installation will fail on windows (Description says that it can not find way to Python even though Python is installed on that pat. Grrr). Workaround is to install this version `web3@^0.20.0`. Oh boy ... 

## Reading
https://truffleframework.com/tutorials/ethereum-overview